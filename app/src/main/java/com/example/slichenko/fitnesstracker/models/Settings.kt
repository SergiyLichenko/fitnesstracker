package com.example.slichenko.fitnesstracker.models

class Settings(val step_size: Float, val weight: Float, val kcal_per_kg_per_m: Float)